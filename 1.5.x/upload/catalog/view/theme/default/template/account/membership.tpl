<?php echo $header; ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="content">
  <table>
	<tr>
	  <td><strong><?php echo $text_membership_group; ?></strong>&nbsp;&nbsp;&nbsp;</td>
	  <td><?php echo $membership_group; ?></td>
	</tr>
	<tr>
	  <td><strong><?php echo $text_expiring; ?></strong></td>
	  <td><?php echo $expiring; ?></td>
	</tr>
	<?php if ((float)$rate) { ?>
	<tr>
	  <td><strong><?php echo $text_rate; ?></strong></td>
	  <td><?php echo $rate; ?>%</td>
	</tr>
	<?php } ?>
  </table>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 