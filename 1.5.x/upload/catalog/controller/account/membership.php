<?php
require_once(substr_replace(DIR_SYSTEM, '', -7) . 'vendor/equotix/customer_group_membership/equotix.php');
class ControllerAccountMembership extends Equotix {
	protected $code = 'customer_group_membership';
	protected $extension_id = '13';
	
	public function index() {
		if (!$this->customer->isLogged() || !$this->validated()) {
	  		$this->session->data['redirect'] = $this->url->link('account/membership', '', 'SSL');
	  
	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	} 
	
		$this->language->load('account/membership');

		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/membership', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_membership_group'] = $this->language->get('text_membership_group');
    	$this->data['text_expiring'] = $this->language->get('text_expiring');
    	$this->data['text_rate'] = $this->language->get('text_rate');
		
		$customer_group_id = $this->customer->getCustomerGroupId();
		
		$this->load->model('account/customer_group');
		
		$customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
		
		$this->data['membership_group'] = $customer_group['name'];
		$this->data['rate'] = $customer_group['rate'];
		
		$order_customer_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_customer_group WHERE customer_id = '" . (int)$this->customer->getId() . "' AND customer_group_id = '" . (int)$customer_group_id . "' ORDER BY date_added DESC LIMIT 1");

		if ($order_customer_group_query->num_rows) {
			$this->data['expiring'] = date($this->language->get('date_format_long'), strtotime($order_customer_group_query->row['date_added']) + ($order_customer_group_query->row['duration'] * 86400));
		} else {
			$this->data['expiring'] = $this->language->get('text_never');
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/membership.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/membership.tpl';
		} else {
			$this->template = 'default/template/account/membership.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'		
		);
				
		$this->response->setOutput($this->render());
  	}
	
	public function cron() {
		if (isset($this->request->get['token']) && $this->request->get['token'] == '0cfa6c079b') {
			$this->language->load('account/membership');
				
			$query = $this->db->query("SELECT (SELECT name FROM " . DB_PREFIX . "customer_group_description cgd WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cgd.customer_group_id = c.customer_group_id) AS customer_group, c.firstname, c.lastname, c.email AS email, ocg.order_customer_group_id AS order_customer_group_id, ocg.customer_id, ocg.duration, ocg.emailed, ocg.date_added AS date_added FROM " . DB_PREFIX . "order_customer_group ocg LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id = ocg.customer_id WHERE expired = '0'");
			
			foreach ($query->rows as $result) {
				if ($this->config->get('customer_group_membership_duration_1') > $this->config->get('customer_group_membership_duration_2')) {
					$first = $this->config->get('customer_group_membership_duration_1');
					$second = $this->config->get('customer_group_membership_duration_2');
					
					$subject = $this->config->get('customer_group_membership_subject_1');
					$message = $this->config->get('customer_group_membership_email_1');
					
					$first_subject = $subject[$this->config->get('config_language_id')];
					$first_message = $message[$this->config->get('config_language_id')];
					
					$subject = $this->config->get('customer_group_membership_subject_2');
					$message = $this->config->get('customer_group_membership_email_2');
					
					$second_subject = $subject[$this->config->get('config_language_id')];
					$second_message = $message[$this->config->get('config_language_id')];
				} else {
					$first = $this->config->get('customer_group_membership_duration_2');
					$second = $this->config->get('customer_group_membership_duration_1');
					
					$subject = $this->config->get('customer_group_membership_subject_2');
					$message = $this->config->get('customer_group_membership_email_2');
					
					$first_subject = $subject[$this->config->get('config_language_id')];
					$first_message = $message[$this->config->get('config_language_id')];
					
					$subject = $this->config->get('customer_group_membership_subject_1');
					$message = $this->config->get('customer_group_membership_email_1');
					
					$second_subject = $subject[$this->config->get('config_language_id')];
					$second_message = $message[$this->config->get('config_language_id')];
				}
				
				$date_added = strtotime($result['date_added']);
				$duration = $result['duration'] * 86400;
				$max = $date_added + $duration;
				$now = time();
				
				$search = array(
					'{firstname}',
					'{lastname}',
					'{expires}',
					'{customer_group}'
				);
				
				$replace = array(
					$result['firstname'],
					$result['lastname'],
					date($this->language->get('date_format_long'), $max),
					$result['customer_group']
				);
				
				if ($now > $max) {
					// Expired
					$this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' WHERE customer_id = '" . (int)$result['customer_id'] . "'");
					
					$this->db->query("UPDATE " . DB_PREFIX . "order_customer_group SET expired = '1' WHERE order_customer_group_id = '" . (int)$result['order_customer_group_id'] . "'");
				} elseif ((($max - $now) / 86400) <= $second && $result['emailed'] == '1') {
					// Expires second (nearing end)
					$second_subject = str_replace($search, $replace, html_entity_decode($second_subject, ENT_QUOTES));
					$second_message = str_replace($search, $replace, html_entity_decode($second_message, ENT_QUOTES));
					
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->hostname = $this->config->get('config_smtp_host');
					$mail->username = $this->config->get('config_smtp_username');
					$mail->password = $this->config->get('config_smtp_password');
					$mail->port = $this->config->get('config_smtp_port');
					$mail->timeout = $this->config->get('config_smtp_timeout');				
					$mail->setTo($result['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($second_subject);
					$mail->setText(strip_tags($second_message));
					$mail->setHtml($second_message);
					$mail->send();
					
					$this->db->query("UPDATE " . DB_PREFIX . "order_customer_group SET emailed = '2' WHERE order_customer_group_id = '" . (int)$result['order_customer_group_id'] . "'");
				} elseif ((($max - $now) / 86400) <= $first && $result['emailed'] == '0') {
					// Expires first (not nearing end)
					$first_subject = str_replace($search, $replace, html_entity_decode($first_subject, ENT_QUOTES));
					$first_message = str_replace($search, $replace, html_entity_decode($first_message, ENT_QUOTES));
					
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->hostname = $this->config->get('config_smtp_host');
					$mail->username = $this->config->get('config_smtp_username');
					$mail->password = $this->config->get('config_smtp_password');
					$mail->port = $this->config->get('config_smtp_port');
					$mail->timeout = $this->config->get('config_smtp_timeout');				
					$mail->setTo($result['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($first_subject);
					$mail->setText(strip_tags($first_message));
					$mail->setHtml($first_message);
					$mail->send();
					
					$this->db->query("UPDATE " . DB_PREFIX . "order_customer_group SET emailed = '1' WHERE order_customer_group_id = '" . (int)$result['order_customer_group_id'] . "'");
				}
			}
		}
	}
}
?>