<?php
require_once(substr_replace(DIR_SYSTEM, '', -7) . 'vendor/equotix/customer_group_membership/equotix.php');
class ControllerModuleCustomerGroupMembership extends Equotix {
	protected $version = '1.4.5';
	protected $code = 'customer_group_membership';
	protected $extension = 'Customer Group Membership';
	protected $extension_id = '13';
	protected $purchase_url = 'customer-group-membership';
	protected $purchase_id = '16281';
	protected $error = array();
	
	public function index() {   
		$this->language->load('module/customer_group_membership');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('customer_group_membership', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['tab_general'] = $this->language->get('tab_general');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_product'] = $this->language->get('text_product');
		$this->data['text_none'] = $this->language->get('text_none');
		
		$this->data['entry_subject_1'] = $this->language->get('entry_subject_1');
		$this->data['entry_email_1'] = $this->language->get('entry_email_1');
		$this->data['entry_duration_1'] = $this->language->get('entry_duration_1');
		$this->data['entry_subject_2'] = $this->language->get('entry_subject_2');
		$this->data['entry_email_2'] = $this->language->get('entry_email_2');
		$this->data['entry_duration_2'] = $this->language->get('entry_duration_2');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_cron'] = $this->language->get('entry_cron');
		$this->data['entry_rebate_type'] = $this->language->get('entry_rebate_type');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/customer_group_membership', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
				
		$this->data['action'] = $this->url->link('module/customer_group_membership', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cron_command'] = 'curl -s -o /dev/null "' . HTTP_CATALOG . 'index.php?route=account/membership/cron&token=0cfa6c079b"';
		
		if (isset($this->request->post['customer_group_membership_subject_1'])) {
			$this->data['customer_group_membership_subject_1'] = $this->request->post['customer_group_membership_subject_1'];
		} elseif ($this->config->get('customer_group_membership_subject_1')) {
			$this->data['customer_group_membership_subject_1'] = $this->config->get('customer_group_membership_subject_1');
		} else {
			$this->data['customer_group_membership_subject_1'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_email_1'])) {
			$this->data['customer_group_membership_email_1'] = $this->request->post['customer_group_membership_email_1'];
		} elseif ($this->config->get('customer_group_membership_email_1')) {
			$this->data['customer_group_membership_email_1'] = $this->config->get('customer_group_membership_email_1');
		} else {
			$this->data['customer_group_membership_email_1'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_duration_1'])) {
			$this->data['customer_group_membership_duration_1'] = $this->request->post['customer_group_membership_duration_1'];
		} elseif ($this->config->get('customer_group_membership_duration_1')) {
			$this->data['customer_group_membership_duration_1'] = $this->config->get('customer_group_membership_duration_1');
		} else {
			$this->data['customer_group_membership_duration_1'] = '';
		}
		
		if (isset($this->request->post['customer_group_membership_subject_2'])) {
			$this->data['customer_group_membership_subject_2'] = $this->request->post['customer_group_membership_subject_2'];
		} elseif ($this->config->get('customer_group_membership_subject_2')) {
			$this->data['customer_group_membership_subject_2'] = $this->config->get('customer_group_membership_subject_2');
		} else {
			$this->data['customer_group_membership_subject_2'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_email_2'])) {
			$this->data['customer_group_membership_email_2'] = $this->request->post['customer_group_membership_email_2'];
		} elseif ($this->config->get('customer_group_membership_email_2')) {
			$this->data['customer_group_membership_email_2'] = $this->config->get('customer_group_membership_email_2');
		} else {
			$this->data['customer_group_membership_email_2'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_duration_2'])) {
			$this->data['customer_group_membership_duration_2'] = $this->request->post['customer_group_membership_duration_2'];
		} elseif ($this->config->get('customer_group_membership_duration_2')) {
			$this->data['customer_group_membership_duration_2'] = $this->config->get('customer_group_membership_duration_2');
		} else {
			$this->data['customer_group_membership_duration_2'] = '';
		}
		
		if (isset($this->request->post['customer_group_membership_rebate_type'])) {
			$this->data['customer_group_membership_rebate_type'] = $this->request->post['customer_group_membership_rebate_type'];
		} elseif ($this->config->get('customer_group_membership_rebate_type')) {
			$this->data['customer_group_membership_rebate_type'] = $this->config->get('customer_group_membership_rebate_type');
		} else {
			$this->data['customer_group_membership_rebate_type'] = '';
		}
		
		if (isset($this->request->post['customer_group_membership_module'])) {
			$this->data['modules'] = $this->request->post['customer_group_membership_module'];
		} elseif ($this->config->get('customer_group_membership_module')) { 
			$this->data['modules'] = $this->config->get('customer_group_membership_module');
		} else {
			$this->data['modules'] = array();
		}
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		$this->generateOutput('module/customer_group_membership.tpl');
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/customer_group_membership') || !$this->validated()) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['customer_group_membership_module'])) {
			foreach ($this->request->post['customer_group_membership_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	public function install() {
		$this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD rate decimal(4,2) NOT NULL DEFAULT '0'");
	
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_group_to_product` (
		  `customer_group_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_group_id` int(11) NOT NULL,
		  `product_id` int(11) NOT NULL,
		  `product_option_value_id` int(11) NOT NULL,
		  `duration` int(11) NOT NULL,
		  PRIMARY KEY (`customer_group_to_product_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_customer_group` (
		  `order_customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_group_to_product_id` int(11) NOT NULL,
		  `order_id` int(11) NOT NULL,
		  `customer_id` int(11) NOT NULL,
		  `customer_group_id` int(11) NOT NULL,
		  `duration` int(11) NOT NULL,
		  `date_added` datetime NOT NULL,
		  `emailed` tinyint(1) NOT NULL,
		  `expired` tinyint(1) NOT NULL,
		  PRIMARY KEY (`order_customer_group_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		
		$this->load->model('setting/setting');
		
		$subject_1[1] = '{customer_group} Membership Expiring in 10 Days';
		$email_1[1] = '<p>Dear {firstname} {lastname},</p>

<p>Your {customer_group} membership will expire on {expires}. We would recommend you to extend your membership with us as soon as possible.</p>

<p>Best regards,<br />
' . $this->config->get('config_name') . '</p>
';

		$subject_2[1] = '{customer_group} Membership Expiring in 2 Days';
		$email_2[1] = '<p>Dear {firstname} {lastname},</p>

<p>Your {customer_group} membership will expire on {expires}. We would recommend you to extend your membership with us as soon as possible.</p>

<p>Best regards,<br />
' . $this->config->get('config_name') . '</p>
';
		
		$data = array(
			'customer_group_membership_subject_1'	=> $subject_1,
			'customer_group_membership_email_1'		=> $email_1,
			'customer_group_membership_duration_1'	=> '10',
			'customer_group_membership_subject_2'	=> $subject_2,
			'customer_group_membership_email_2'		=> $email_2,
			'customer_group_membership_duration_2'	=> '2',
		);
		
		$this->model_setting_setting->editSetting('customer_group_membership', $data);
	}
	
	public function uninstall() {
		$this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group DROP rate");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "customer_group_to_product`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "order_customer_group`");
	}
}
?>