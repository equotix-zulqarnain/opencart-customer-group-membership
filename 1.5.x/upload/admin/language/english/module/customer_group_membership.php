<?php
// Heading
$_['heading_title']       = 'Customer Group Membership';

// Tab
$_['tab_general']		  = 'General';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Customer Group Membership!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_total']   		  = 'Rebate Order Total (Includes taxes &amp; shipping)';
$_['text_product']   	  = 'Per Product (Excludes taxes &amp; shipping)';

// Entry
$_['entry_subject_1']	  = 'First Email Subject:';
$_['entry_email_1']		  = 'First Email Message:';
$_['entry_duration_1']	  = 'Duration before Membership Expires:';
$_['entry_subject_2']	  = 'Second Email Subject:';
$_['entry_email_2']		  = 'Second Email Message:';
$_['entry_duration_2']	  = 'Duration before Membership Expires:';
$_['entry_limit']         = 'Limit:'; 
$_['entry_image']         = 'Image (W x H) and Resize Type:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_cron']    	  = 'Cron Command:';
$_['entry_rebate_type']   = 'Rebate Type:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Customer Group Membership!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>