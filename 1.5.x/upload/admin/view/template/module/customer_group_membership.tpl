<?php echo $header; ?>
<div id="content">
  <div class="container-fluid">
    <div class="pull-right">
      <button type="submit" form="form-module" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-module" class="form-horizontal">
		  <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-about" data-toggle="tab"><i class="fa fa-question-circle"></i> About</a></li>
          </ul>
		  <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-subject-1"><?php echo $entry_subject_1; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
					<input type="text" name="customer_group_membership_subject_1[<?php echo $language['language_id']; ?>]" value="<?php echo isset($customer_group_membership_subject_1[$language['language_id']]) ? $customer_group_membership_subject_1[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_subject_1; ?>" class="form-control" />
				  </div>
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-email-1"><?php echo $entry_email_1; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <textarea name="customer_group_membership_email_1[<?php echo $language['language_id']; ?>]" id="email-1-<?php echo $language['language_id']; ?>"><?php echo isset($customer_group_membership_email_1[$language['language_id']]) ? $customer_group_membership_email_1[$language['language_id']] : ''; ?></textarea>
				  <br /><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-duration-1"><?php echo $entry_duration_1; ?></label>
                <div class="col-sm-10">
				  <input type="text" name="customer_group_membership_duration_1" value="<?php echo $customer_group_membership_duration_1; ?>" placeholder="<?php echo $entry_duration_1; ?>" class="form-control" />
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-subject-2"><?php echo $entry_subject_2; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
					<input type="text" name="customer_group_membership_subject_2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($customer_group_membership_subject_2[$language['language_id']]) ? $customer_group_membership_subject_2[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_subject_2; ?>" class="form-control" />
				  </div>
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-email-2"><?php echo $entry_email_2; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <textarea name="customer_group_membership_email_2[<?php echo $language['language_id']; ?>]" id="email-2-<?php echo $language['language_id']; ?>"><?php echo isset($customer_group_membership_email_2[$language['language_id']]) ? $customer_group_membership_email_2[$language['language_id']] : ''; ?></textarea>
				  <br /><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-duration-2"><?php echo $entry_duration_2; ?></label>
                <div class="col-sm-10">
				  <input type="text" name="customer_group_membership_duration_2" value="<?php echo $customer_group_membership_duration_2; ?>" placeholder="<?php echo $entry_duration_2; ?>" class="form-control" />
				</div>
              </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-rebate"><?php echo $entry_rebate_type; ?></label>
				<div class="col-sm-10">
				  <select name="customer_group_membership_rebate_type" id="input-rebate" class="form-control">
					<option value="0"<?php echo $customer_group_membership_rebate_type == '0' ? ' selected="selected"' : ''; ?>><?php echo $text_none; ?></option>
					<option value="1"<?php echo $customer_group_membership_rebate_type == '1' ? ' selected="selected"' : ''; ?>><?php echo $text_total; ?></option>
					<option value="2"<?php echo $customer_group_membership_rebate_type == '2' ? ' selected="selected"' : ''; ?>><?php echo $text_product; ?></option>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-cron"><?php echo $entry_cron; ?></label>
				<div class="col-sm-10">
				  <textarea rows="3" class="form-control" readonly="true"><?php echo $cron_command; ?></textarea>
				</div>
			  </div>
			  <table id="module" class="table table-bordered table-striped table-hover">
				<thead>
				<tr>
				  <td class="text-left"><?php echo $entry_limit; ?></td>
				  <td class="text-left"><?php echo $entry_image; ?></td>
				  <td class="text-left"><?php echo $entry_layout; ?></td>
				  <td class="text-left"><?php echo $entry_position; ?></td>
				  <td class="text-left"><?php echo $entry_status; ?></td>
				  <td class="text-right"><?php echo $entry_sort_order; ?></td>
				  <td></td>
				</tr>
				</thead>
				<?php $module_row = 0; ?>
				<?php foreach ($modules as $module) { ?>
				<tbody id="module-row<?php echo $module_row; ?>">
				<tr>
				  <td class="text-left"><input type="text" name="customer_group_membership_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="1" class="form-control" /></td>
				  <td class="text-left"><input type="text" name="customer_group_membership_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" class="form-control" />
					<input type="text" name="customer_group_membership_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" class="form-control" />
					<?php if (isset($error_image[$module_row])) { ?>
					<span class="error"><?php echo $error_image[$module_row]; ?></span>
					<?php } ?></td>
				  <td class="text-left"><select name="customer_group_membership_module[<?php echo $module_row; ?>][layout_id]" class="form-control">
					  <?php foreach ($layouts as $layout) { ?>
					  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
					  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
					  <?php } else { ?>
					  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
					  <?php } ?>
					  <?php } ?>
					</select></td>
				  <td class="text-left"><select name="customer_group_membership_module[<?php echo $module_row; ?>][position]" class="form-control">
					  <?php if ($module['position'] == 'content_top') { ?>
					  <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
					  <?php } else { ?>
					  <option value="content_top"><?php echo $text_content_top; ?></option>
					  <?php } ?>
					  <?php if ($module['position'] == 'content_bottom') { ?>
					  <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
					  <?php } else { ?>
					  <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
					  <?php } ?>
					  <?php if ($module['position'] == 'column_left') { ?>
					  <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
					  <?php } else { ?>
					  <option value="column_left"><?php echo $text_column_left; ?></option>
					  <?php } ?>
					  <?php if ($module['position'] == 'column_right') { ?>
					  <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
					  <?php } else { ?>
					  <option value="column_right"><?php echo $text_column_right; ?></option>
					  <?php } ?>
					</select></td>
				  <td class="text-left"><select name="customer_group_membership_module[<?php echo $module_row; ?>][status]" class="form-control">
					  <?php if ($module['status']) { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					  <option value="0"><?php echo $text_disabled; ?></option>
					  <?php } else { ?>
					  <option value="1"><?php echo $text_enabled; ?></option>
					  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					  <?php } ?>
					</select></td>
				  <td class="text-right"><input type="text" name="customer_group_membership_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" class="form-control" /></td>
				  <td class="text-left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>
				</tr>
				</tbody>
				<?php $module_row++; ?>
				<?php } ?>
				<tfoot>
				<tr>
				  <td colspan="6"></td>
				  <td class="text-left"><a onclick="addModule();" class="btn btn-success"><?php echo $button_add_module; ?></a></td>
				</tr>
				</tfoot>
			  </table>
			</div>
			<?php require_once(substr_replace(DIR_SYSTEM, '', -7) . 'vendor/equotix/' . $code . '/equotix.tpl'); ?>
		  </div>
		</form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('email-1-<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});

CKEDITOR.replace('email-2-<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="text-left"><input type="text" name="customer_group_membership_module[' + module_row + '][limit]" value="5" size="1" class="form-control" /></td>';
	html += '    <td class="text-left"><input type="text" name="customer_group_membership_module[' + module_row + '][image_width]" value="80" size="3" class="form-control" /> <input type="text" name="customer_group_membership_module[' + module_row + '][image_height]" value="80" size="3" class="form-control" /></td>';
	html += '    <td class="text-left"><select name="customer_group_membership_module[' + module_row + '][layout_id]" class="form-control">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="text-left"><select name="customer_group_membership_module[' + module_row + '][position]" class="form-control">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="text-left"><select name="customer_group_membership_module[' + module_row + '][status]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-right"><input type="text" name="customer_group_membership_module[' + module_row + '][sort_order]" value="" size="3" class="form-control" /></td>';
	html += '    <td class="text-left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<?php echo $footer; ?>