<?php
class ControllerExtensionModuleCustomerGroupMembership extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/customer_group_membership');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$products_query = $this->db->query("SELECT DISTINCT(product_id) FROM " . DB_PREFIX . "customer_group_to_product");

		$products = array();

		foreach ($products_query->rows as $product) {
			$products[] = $product['product_id'];
		}
		
		shuffle($products);

		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}
	
		$products = array_slice($products, 0, (int)$setting['limit']);

		if ($products) {
			foreach ($products as $product_id) {
				$result = $this->model_catalog_product->getProduct($product_id);
			
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, (version_compare(VERSION, '2.2.0.0', '<') ? $this->config->get('config_product_description_length') : $this->config->get($this->config->get('config_theme') . '_product_description_length'))) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}


			return $this->load->view('extension/module/customer_group_membership', $data);
		}
	}
	
	public function eventPreViewAccountAccount($route, &$data) {
		$this->load->language('extension/module/customer_group_membership');
		
		$data['text_membership'] = $this->language->get('text_membership');
		$data['membership'] = $this->url->link('extension/module/membership', '', 'SSL');
	}
	
	public function eventPostModelCheckoutOrderAddOrderHistory($route, $args, $output) {
		$order_id = $args[0];
		$order_status_id = $args[1];

		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($order_id);
		
		if (!$order_info || !$order_info['customer_id']) {
			return;
		}
		
		if (in_array($order_status_id, $this->config->get('config_complete_status'))) {
			$order_products = $this->model_checkout_order->getOrderProducts($order_id);
			
			// Add rebates first, so the customer group wouldn't change first
			if (in_array($order_status_id, $this->config->get('config_complete_status'))) {
				if ($order_info['customer_id']) {
					$customer_query = $this->db->query("SELECT cg.rate FROM " . DB_PREFIX . "customer_group_membership_rate cg LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_group_id = cg.customer_group_id WHERE c.customer_id = '" . (int)$order_info['customer_id'] . "'");
					
					if ((float)$customer_query->row['rate']) {
						if ($this->config->get('customer_group_membership_rebate_type') == '0') {
							$amount = 0;
						} elseif ($this->config->get('customer_group_membership_rebate_type') == '1') {
							$amount = $order_info['total'] * (float)$customer_query->row['rate'] / 100;
						} else {
							$amount = 0;
							
							foreach ($order_products as $product) {
								$amount += $product['price'] * $product['quantity'] * (float)$customer_query->row['rate'] / 100;
							}
						}
						
						$transaction_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$order_info['customer_id'] . "' AND order_id = '" . (int)$order_info['order_id'] . "' AND description LIKE 'Order #%'");
						
						if (!$transaction_query->num_rows) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', description = 'Order #" . (int)$order_info['order_id'] . "', amount = '" . (float)$amount . "', date_added = NOW()");
						}
					}
				}
				
				$customer_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_customer_group WHERE order_id = '" . (int)$order_id . "' LIMIT 1");
				
				if ($customer_group_query->num_rows) {
					$this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_query->row['customer_group_id'] . "' WHERE customer_id = '" . (int)$order_info['customer_id'] . "'");
				}
			}
			
			// Assign customer group
			foreach ($order_products as $order_product) {
				$order_options = $this->model_checkout_order->getOrderOptions($order_id, $order_product['order_product_id']);
				
				$exists = false;
				
				foreach ($order_options as $option) {
					$customer_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_to_product WHERE product_id = '" . (int)$order_product['product_id'] . "' AND product_option_value_id = '" . (int)$option['product_option_value_id'] . "' ORDER BY duration DESC LIMIT 1");
				
					if ($customer_group_query->num_rows) {
						$exists = true;
						
						break;
					}
				}
				
				if (!$exists) {
					$customer_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_to_product WHERE product_id = '" . (int)$order_product['product_id'] . "' AND product_option_value_id = '0' ORDER BY duration DESC LIMIT 1");
				}
				
				$customer_group_membership = false;
			
				if ($customer_group_query->num_rows) {
					$customer_group_membership = true;
					
					// Check if customer already in this group, if so, adjust the date added value to the date end value of the other membership
					$check_customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$order_info['customer_id'] . "'");
					
					if ($check_customer_query->num_rows && $check_customer_query->row['customer_group_id'] == $customer_group_query->row['customer_group_id']) {
						$order_customer_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_customer_group WHERE order_id != '" . (int)$order_id . "' AND customer_id = '" . (int)$order_info['customer_id'] . "' AND customer_group_id = '" . (int)$customer_group_query->row['customer_group_id'] . "' ORDER BY date_added DESC LIMIT 1");
						
						if ($order_customer_group_query->num_rows) {
							$date_added = date('Y-m-d', strtotime($order_customer_group_query->row['date_added']) + (($order_customer_group_query->row['duration'] + 1)* 86400));
						} else {
							$date_added = date('Y-m-d', strtotime($order_info['date_added']));
						}
					} else {
						$date_added = date('Y-m-d', strtotime($order_info['date_added']));
					}
					
					// Delete ALL old records so it will stop reminding
					$this->db->query("DELETE FROM " . DB_PREFIX . "order_customer_group WHERE customer_id = '" . (int)$order_info['customer_id'] . "'");
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_customer_group SET customer_group_to_product_id = '" . (int)$customer_group_query->row['customer_group_to_product_id'] . "', order_id = '" . (int)$order_id . "', customer_id = '" . (int)$order_info['customer_id'] . "', customer_group_id = '" . (int)$customer_group_query->row['customer_group_id'] . "', duration = '" . (int)$customer_group_query->row['duration']. "', date_added = '" . $this->db->escape($date_added) . "'");
				
					if ($customer_group_membership) {
						if (in_array($order_status_id, $this->config->get('config_complete_status'))) {
							$this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_query->row['customer_group_id'] . "' WHERE customer_id = '" . (int)$order_info['customer_id'] . "'");
						}
					}
				}
			}
		}
	}
}