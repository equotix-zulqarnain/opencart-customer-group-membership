<?php
require_once(DIR_SYSTEM . 'library/equotix/customer_group_membership/equotix.php');
class ControllerExtensionModuleCustomerGroupMembership extends Equotix {
	protected $version = '2.0.1';
	protected $code = 'customer_group_membership';
	protected $extension = 'Customer Group Membership';
	protected $extension_id = '13';
	protected $purchase_url = 'customer-group-membership';
	protected $purchase_id = '16281';
	protected $error = array();

	public function index() {
		$this->load->language('extension/module/customer_group_membership');
        
		$data['oc_ver3_1'] = version_compare(VERSION, "3.0.3.2", ">");

		if ($data['oc_ver3_1']) {
			$this->document->addScript('view/javascript/ckeditor/ckeditor.js');
			$this->document->addScript('view/javascript/ckeditor/adapters/jquery.js');
		} else {
			$this->document->addScript('view/javascript/summernote/summernote.js');
			$this->document->addScript('view/javascript/summernote/opencart.js');
			$this->document->addStyle('view/javascript/summernote/summernote.css');
		}

		$this->document->setTitle(strip_tags($this->language->get('heading_title')));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->addModule('module_customer_group_membership', $this->request->post);
			
			$this->model_setting_setting->editSetting('module_customer_group_membership', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
		
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
	
		$data['entry_subject_1'] = $this->language->get('entry_subject_1');
		$data['entry_email_1'] = $this->language->get('entry_email_1');
		$data['entry_duration_1'] = $this->language->get('entry_duration_1');
		$data['entry_subject_2'] = $this->language->get('entry_subject_2');
		$data['entry_email_2'] = $this->language->get('entry_email_2');
		$data['entry_duration_2'] = $this->language->get('entry_duration_2');
		$data['entry_cron'] = $this->language->get('entry_cron');
		$data['entry_rebate_type'] = $this->language->get('entry_rebate_type');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled']	= $this->language->get('text_enabled');
		$data['text_disabled']	= $this->language->get('text_disabled');
		$data['text_total'] = $this->language->get('text_total');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_none'] = $this->language->get('text_none');
		
		$data['tab_general'] = $this->language->get('tab_general');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_module_add'] = $this->language->get('button_module_add');
		$data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/customer_group_membership', 'user_token=' . $this->session->data['user_token'], true)
   		);
		
		$data['action'] = $this->url->link('extension/module/customer_group_membership', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		
		$data['cron_command'] = 'curl -s -o /dev/null "' . HTTP_CATALOG . 'index.php?route=extension/module/membership/cron&token=0cfa6c079b"';
		
		if (isset($this->request->post['module_customer_group_membership_subject_1'])) {
			$data['module_customer_group_membership_subject_1'] = $this->request->post['module_customer_group_membership_subject_1'];
		} elseif ($this->config->get('module_customer_group_membership_subject_1')) {
			$data['module_customer_group_membership_subject_1'] = $this->config->get('module_customer_group_membership_subject_1');
		} else {
			$data['module_customer_group_membership_subject_1'] = array();
		}
		
		if (isset($this->request->post['module_customer_group_membership_email_1'])) {
			$data['module_customer_group_membership_email_1'] = $this->request->post['module_customer_group_membership_email_1'];
		} elseif ($this->config->get('module_customer_group_membership_email_1')) {
			$data['module_customer_group_membership_email_1'] = $this->config->get('module_customer_group_membership_email_1');
		} else {
			$data['module_customer_group_membership_email_1'] = array();
		}
		
		if (isset($this->request->post['module_customer_group_membership_duration_1'])) {
			$data['module_customer_group_membership_duration_1'] = $this->request->post['module_customer_group_membership_duration_1'];
		} elseif ($this->config->get('module_customer_group_membership_duration_1')) {
			$data['module_customer_group_membership_duration_1'] = $this->config->get('module_customer_group_membership_duration_1');
		} else {
			$data['module_customer_group_membership_duration_1'] = '';
		}
		
		if (isset($this->request->post['module_customer_group_membership_subject_2'])) {
			$data['module_customer_group_membership_subject_2'] = $this->request->post['module_customer_group_membership_subject_2'];
		} elseif ($this->config->get('module_customer_group_membership_subject_2')) {
			$data['module_customer_group_membership_subject_2'] = $this->config->get('module_customer_group_membership_subject_2');
		} else {
			$data['module_customer_group_membership_subject_2'] = array();
		}
		
		if (isset($this->request->post['module_customer_group_membership_email_2'])) {
			$data['module_customer_group_membership_email_2'] = $this->request->post['module_customer_group_membership_email_2'];
		} elseif ($this->config->get('module_customer_group_membership_email_2')) {
			$data['module_customer_group_membership_email_2'] = $this->config->get('module_customer_group_membership_email_2');
		} else {
			$data['module_customer_group_membership_email_2'] = array();
		}
		
		if (isset($this->request->post['module_customer_group_membership_duration_2'])) {
			$data['module_customer_group_membership_duration_2'] = $this->request->post['module_customer_group_membership_duration_2'];
		} elseif ($this->config->get('module_customer_group_membership_duration_2')) {
			$data['module_customer_group_membership_duration_2'] = $this->config->get('module_customer_group_membership_duration_2');
		} else {
			$data['module_customer_group_membership_duration_2'] = '';
		}
		
		if (isset($this->request->post['module_customer_group_membership_rebate_type'])) {
			$data['module_customer_group_membership_rebate_type'] = $this->request->post['module_customer_group_membership_rebate_type'];
		} elseif ($this->config->get('module_customer_group_membership_rebate_type')) {
			$data['module_customer_group_membership_rebate_type'] = $this->config->get('module_customer_group_membership_rebate_type');
		} else {
			$data['module_customer_group_membership_rebate_type'] = '';
		}
		
		if (isset($this->request->post['module_customer_group_membership_module'])) {
			$modules = $this->request->post['module_customer_group_membership_module'];
		} elseif ($this->config->get('module_customer_group_membership_module')) {
			$modules = $this->config->get('module_customer_group_membership_module');
		} else {
			$modules = array();
		}
		
		$data['module_customer_group_membership_module'] = array();
		
		foreach ($modules as $key => $module) {
			$data['module_customer_group_membership_module'][] = array(
				'key'    => $key,
				'limit'  => $module['limit'],
				'width'  => $module['width'],
				'height' => $module['height']
			);
		}
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->generateOutput('extension/module/customer_group_membership', $data);
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/customer_group_membership') || !$this->validated()) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	protected function addModule($code, $data) {
		if (isset($data[$code . '_module'])) {
			unset($this->request->post[$code . '_status']);
			
			$this->load->model('setting/module');
			
			$modules = $this->model_setting_module->getModulesByCode($this->code);
			
			$module_data = array();
			
			foreach ($modules as $module) {
				$module_setting = json_decode($module['setting'], true);
				
				if (!empty($module_setting['row_id'])) {
					$module_data[$module_setting['row_id']] = $module['module_id'];
				} else {
					$this->model_setting_module->deleteModule($module['module_id']);
				}
			}
			
			foreach ($data[$code . '_module'] as $key => $value) {
				$module_setting = array();
				
				$module_setting['name'] = 'Module ' . $key;
				$module_setting['status'] = 1;
				$module_setting['row_id'] = $key;
				
				foreach ($value as $key1 => $value1) {
					$module_setting[$key1] = $value1;
				}
				
				if (!empty($module_data[$key])) {
					$this->model_setting_module->editModule($module_data[$key], $module_setting);
					
					unset($module_data[$key]);
				} else {
					$this->model_setting_module->addModule($this->code, $module_setting);
				}
			}

			foreach ($module_data as $module_id) {
				$this->model_setting_module->deleteModule($module_id);
			}
		}
	}
	
	public function install() {
		if (!$this->user->hasPermission('modify', 'extension/extension/module')) {
			return;
		}
				
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_group_membership_rate` (
		  `customer_group_id` int(11) NOT NULL,
		  `rate` decimal(4,2) NOT NULL,
		  PRIMARY KEY (`customer_group_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
	
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_group_to_product` (
		  `customer_group_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_group_id` int(11) NOT NULL,
		  `product_id` int(11) NOT NULL,
		  `product_option_value_id` int(11) NOT NULL,
		  `duration` int(11) NOT NULL,
		  PRIMARY KEY (`customer_group_to_product_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_customer_group` (
		  `order_customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_group_to_product_id` int(11) NOT NULL,
		  `order_id` int(11) NOT NULL,
		  `customer_id` int(11) NOT NULL,
		  `customer_group_id` int(11) NOT NULL,
		  `duration` int(11) NOT NULL,
		  `date_added` datetime NOT NULL,
		  `emailed` tinyint(1) NOT NULL,
		  `expired` tinyint(1) NOT NULL,
		  PRIMARY KEY (`order_customer_group_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		
		$this->load->model('setting/setting');
		
		$subject_1[1] = '{customer_group} Membership Expiring in 10 Days';
		$email_1[1] = '<p>Dear {firstname} {lastname},</p>

<p>Your {customer_group} membership will expire on {expires}. We would recommend you to extend your membership with us as soon as possible.</p>

<p>Best regards,<br />
' . $this->config->get('config_name') . '</p>
';

		$subject_2[1] = '{customer_group} Membership Expiring in 2 Days';
		$email_2[1] = '<p>Dear {firstname} {lastname},</p>

<p>Your {customer_group} membership will expire on {expires}. We would recommend you to extend your membership with us as soon as possible.</p>

<p>Best regards,<br />
' . $this->config->get('config_name') . '</p>
';

		$this->load->model('setting/setting');
		
		$data = array(
			'module_customer_group_membership_subject_1'	=> $subject_1,
			'module_customer_group_membership_email_1'		=> $email_1,
			'module_customer_group_membership_duration_1'	=> '10',
			'module_customer_group_membership_subject_2'	=> $subject_2,
			'module_customer_group_membership_email_2'		=> $email_2,
			'module_customer_group_membership_duration_2'	=> '2',
		);
		
		$this->model_setting_setting->editSetting('module_customer_group_membership', $data);
		
		$this->load->model('setting/event');
		
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/view/customer/customer_form/before', 'extension/module/customer_group_membership/eventPreViewCustomerCustomerForm');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/view/customer/customer_group_form/before', 'extension/module/customer_group_membership/eventPreViewCustomerCustomerGroupForm');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer/addCustomer/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerAddEdit');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer/editCustomer/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerAddEdit');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer/deleteCustomer/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerDelete');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer_group/addCustomerGroup/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerGroupAddEdit');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer_group/editCustomerGroup/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerGroupAddEdit');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer_group/deleteCustomerGroup/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerGroupDelete');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'admin/model/customer/customer_group/getCustomerGroup/after', 'extension/module/customer_group_membership/eventPostModelCustomerCustomerGroupGetCustomerGroup');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/module/customer_group_membership/eventPostModelCheckoutOrderAddOrderHistory');
		$this->model_setting_event->addEvent('module_customer_group_membership', 'catalog/view/account/account/before', 'extension/module/customer_group_membership/eventPreViewAccountAccount');
	}
	
	public function uninstall() {
		if (!$this->user->hasPermission('modify', 'extension/extension/module')) {
			return;
		}
		
		$this->db->query("DROP TABLE `" . DB_PREFIX . "customer_group_membership_rate`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "customer_group_to_product`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "order_customer_group`");
		
		$this->load->model('setting/event');
		
		$this->model_setting_event->deleteEventByCode('module_customer_group_membership');
	}
	
	public function product() {
		$json = array();
		
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/product');
		
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}
			
			$data = array(
				'filter_name'  => $filter_name,
				'start'        => 0,
				'limit'        => 20
			);
			
			$results = $this->model_catalog_product->getProducts($data);
			
			foreach ($results as $result) {
				$options_query = $this->db->query("SELECT od.name AS `option`, ovd.name AS name, pov.product_option_value_id AS product_option_value_id FROM " . DB_PREFIX . "product_option_value pov  LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON pov.option_value_id = ovd.option_value_id LEFT JOIN " . DB_PREFIX . "option_description od ON od.option_id = pov.option_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND od.language_id ='" . (int)$this->config->get('config_language_id') . "' AND product_id = '" . (int)$result['product_id'] . "'");
		
				$options = array();
				
				foreach ($options_query->rows as $option) {
					$options[] = array(
						'product_option_value_id'	=> $option['product_option_value_id'],
						'name'						=> $option['option'] . ' - ' . $option['name']
					);
				}
			
				$json[] = array(
					'product_id'	=> $result['product_id'],
					'product'		=> html_entity_decode($result['name']),
					'options'		=> $options
				);
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function eventPreViewCustomerCustomerForm($route, &$data) {
		$this->load->language('extension/module/customer_group_membership');

		$data['entry_customer_group_expiry'] = $this->language->get('entry_customer_group_expiry');
		$data['entry_customer_group_duration'] = $this->language->get('entry_customer_group_duration');
		
		if (isset($this->request->get['customer_id'])) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->request->get['customer_id'] . "'");
			
			$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_customer_group WHERE customer_id = '" . (int)$this->request->get['customer_id'] . "' AND expired = '0' AND customer_group_id = '" . (int)$query->row['customer_group_id'] . "' ORDER BY date_added DESC LIMIT 1");

			if ($query1->num_rows) {
				$customer_group_expiry = date('Y-m-d', (strtotime($query1->row['date_added']) + ($query1->row['duration'] * 86400)));
				$customer_group_duration = $query1->row['duration'];
				$order_customer_group_id = $query1->row['order_customer_group_id'];
			}
		}
	
		if (isset($this->request->post['customer_group_expiry'])) {
			$data['customer_group_expiry'] = $this->request->post['customer_group_expiry'];
		} elseif (isset($customer_group_expiry)) {
			$data['customer_group_expiry'] = $customer_group_expiry;
		} else {
			$data['customer_group_expiry'] = '';
		}
		
		if (isset($this->request->post['customer_group_duration'])) {
			$data['customer_group_duration'] = $this->request->post['customer_group_duration'];
		} elseif (isset($customer_group_duration)) {
			$data['customer_group_duration'] = $customer_group_duration;
		} else {
			$data['customer_group_duration'] = '';
		}
		
		if (isset($this->request->post['order_customer_group_id'])) {
			$data['order_customer_group_id'] = $this->request->post['order_customer_group_id'];
		} elseif (isset($order_customer_group_id)) {
			$data['order_customer_group_id'] = $order_customer_group_id;
		} else {
			$data['order_customer_group_id'] = 0;
		}
	}
	
	public function eventPostModelCustomerCustomerAddEdit($route, $args, $output) {
		if (isset($args[1])) {
			$customer_id = $args[0];
			$data = $args[1];
		} else {
			$customer_id = $output;
			$data = $args[1];
		}
	
		if (!empty($data['customer_group_expiry']) && $data['customer_group_expiry'] != '0000-00-00') {
			$expiry = date('Y-m-d', (strtotime($data['customer_group_expiry']) - ($data['customer_group_duration'] * 86400)));
		
			if (!empty($data['order_customer_group_id'])) {
				$this->db->query("UPDATE " . DB_PREFIX . "order_customer_group SET customer_group_id = '" . (int)$data['customer_group_id'] . "', duration = '" . (int)$data['customer_group_duration'] . "', date_added = '" . $this->db->escape($expiry) . "' WHERE order_customer_group_id = '" . (int)$data['order_customer_group_id'] . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_customer_group SET customer_id = '" . (int)$customer_id . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', duration = '" . (int)$data['customer_group_duration'] . "', date_added = '" . $this->db->escape($expiry) . "'");
			}
		}
	}
	
	public function eventPostModelCustomerCustomerDelete($route, $args, $output) {
		$customer_id = $args[0];
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_customer_group WHERE customer_id = '" . (int)$customer_id . "'");
	}
	
	public function eventPreViewCustomerCustomerGroupForm($route, &$data) {
		$this->load->language('extension/module/customer_group_membership');
		
		$data['entry_rate'] = $this->language->get('entry_rate');
		
		$data['column_product'] = $this->language->get('column_product');
		$data['column_option'] = $this->language->get('column_option');
		$data['column_duration'] = $this->language->get('column_duration');
		
		$data['button_add'] = $this->language->get('button_add');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['text_none'] = $this->language->get('text_none');
		
		$data['user_token'] = $this->session->data['user_token'];
		
		$this->load->model('customer/customer_group');
		
		if (!empty($this->request->get['customer_group_id'])) {
			$customer_group_info = $this->model_customer_customer_group->getCustomerGroup($this->request->get['customer_group_id']);
		} else {
			$customer_group_info = array();
		}
		
		if (isset($this->request->post['rate'])) {
			$data['rate'] = $this->request->post['rate'];
		} elseif (!empty($customer_group_info)) {
			$data['rate'] = $customer_group_info['rate'];
		} else {
			$data['rate'] = '';
		}
		
		if (isset($this->request->post['products'])) {
			$products = $this->request->post['products'];
		} elseif (!empty($customer_group_info)) {
			$products = $customer_group_info['products'];
		} else {
			$products = array();
		}
		
		$data['products'] = array();
		
		foreach ($products as $product) {
			if (isset($product['options'])) {
				$options = $product['options'];
			} else {
				$options_query = $this->db->query("SELECT od.name AS `option`, ovd.name AS name, pov.product_option_value_id AS product_option_value_id FROM " . DB_PREFIX . "product_option_value pov  LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON pov.option_value_id = ovd.option_value_id LEFT JOIN " . DB_PREFIX . "option_description od ON od.option_id = pov.option_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' AND product_id = '" . (int)$product['product_id'] . "'");
			
				$options = array();
				
				foreach ($options_query->rows as $option) {
					$options[] = array(
						'product_option_value_id'	=> $option['product_option_value_id'],
						'name'						=> $option['option'] . ' - ' . $option['name']
					);
				}
			}
		
			$data['products'][] = array(
				'product_id'				=> $product['product_id'],
				'product'					=> $product['product'],
				'options'					=> $options,
				'product_option_value_id'	=> $product['product_option_value_id'],
				'duration'					=> $product['duration']
			);
		}
	}
	
	public function eventPostModelCustomerCustomerGroupAddEdit($route, $args, $output) {
		if (isset($args[1])) {
			$customer_group_id = $args[0];
			$data = $args[1];
		} else {
			$customer_group_id = $output;
			$data = $args[0];
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_membership_rate WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_to_product WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_membership_rate SET rate = '" . (float)$data['rate'] . "', customer_group_id = '" . (int)$customer_group_id . "'");
		
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_to_product SET customer_group_id = '" . (int)$customer_group_id . "', product_id = '" . (int)$product['product_id'] . "', product_option_value_id = '" . (int)$product['product_option_value_id'] . "', duration = '" . (int)$product['duration'] . "'");
			}
		}
	}
	
	public function eventPostModelCustomerCustomerGroupDelete($route, $args, $output) {
		$customer_group_id = $args[0];
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_membership_rate WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_to_product WHERE customer_group_id = '" . (int)$customer_group_id . "'");
	}
	
	public function eventPostModelCustomerCustomerGroupGetCustomerGroup($route, $args, &$output) {
		$customer_group_id = $args[0];
		
		$query1 = $this->db->query("SELECT *, (SELECT pd.name FROM " . DB_PREFIX . "product_description pd WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pd.product_id = cg2p.product_id) AS product FROM " . DB_PREFIX . "customer_group_to_product cg2p WHERE cg2p.customer_group_id = '" . (int)$customer_group_id . "'");
		
		$output['products'] = array();
		
		foreach ($query1->rows as $result) {
			$options_query = $this->db->query("SELECT od.name AS `option`, ovd.name AS name, pov.product_option_value_id AS product_option_value_id FROM " . DB_PREFIX . "product_option_value pov  LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON pov.option_value_id = ovd.option_value_id LEFT JOIN " . DB_PREFIX . "option_description od ON od.option_id = pov.option_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' AND product_id = '" . (int)$result['product_id'] . "'");
			
			$options = array();
			
			foreach ($options_query->rows as $option) {
				$options[] = array(
					'product_option_value_id'	=> $option['product_option_value_id'],
					'name'						=> $option['option'] . ' - ' . $option['name']
				);
			}
			
			$output['products'][] = array(
				'product_id'				=> $result['product_id'],
				'product'					=> $result['product'],
				'product_option_value_id'	=> $result['product_option_value_id'],
				'options'					=> $options,
				'duration'					=> $result['duration']
			);
		}
		
		$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_membership_rate WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		
		$output['rate'] = $query1->num_rows ? $query1->row['rate'] : 0;
	}
}