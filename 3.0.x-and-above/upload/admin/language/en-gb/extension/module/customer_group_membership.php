<?php
// Heading
$_['heading_title']    			= '<span style="color:#006bb3;">Customer Group Membership</span>';

// Text
$_['text_module']      			= 'Modules';
$_['text_success']     			= 'Success: You have modified module Customer Group Membership!';
$_['text_edit']					= 'Edit Module Customer Group Membership';
$_['text_total']   		  		= 'Rebate Order Total (Includes taxes &amp; shipping)';
$_['text_product']   	  		= 'Per Product (Excludes taxes &amp; shipping)';

// Entry
$_['entry_subject_1']	  		= 'First Email Subject';
$_['entry_email_1']		  		= 'First Email Message';
$_['entry_duration_1']	  		= 'Duration before Membership Expires';
$_['entry_subject_2']	  		= 'Second Email Subject';
$_['entry_email_2']		 		= 'Second Email Message';
$_['entry_duration_2']	  		= 'Duration before Membership Expires';
$_['entry_limit']         		= 'Limit'; 
$_['entry_image']         		= 'Image (W x H)';
$_['entry_cron']    	  		= 'Cron Command';
$_['entry_rebate_type']   		= 'Rebate Type';
$_['entry_width']   			= 'Width';
$_['entry_height']   			= 'Height';
$_['entry_customer_group_expiry'] 	= 'Customer Group Expiry';
$_['entry_customer_group_duration'] = 'Customer Group Duration';
$_['entry_rate'] 				= 'Rebate Credits Rate (%):';

// Column
$_['column_product'] 			= 'Product:';
$_['column_option'] 			= 'Option Value:';
$_['column_duration'] 			= 'Duration (Days):';

// Button
$_['button_add'] 				= 'Add Product';

// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify module Customer Group Membership!';