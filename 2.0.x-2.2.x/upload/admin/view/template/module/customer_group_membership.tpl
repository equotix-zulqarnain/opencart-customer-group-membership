<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-module" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-module" class="form-horizontal">
		  <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <?php echo $about; ?>
          </ul>
		  <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-subject-1"><?php echo $entry_subject_1; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="<?php echo version_compare(VERSION, '2.2.0.0', '<') ? 'view/image/flags/' . $language['image'] : 'language/' . $language['code'] . '/' . $language['code'] . '.png'; ?>" title="<?php echo $language['name']; ?>" /></span>
					<input type="text" name="customer_group_membership_subject_1[<?php echo $language['language_id']; ?>]" value="<?php echo isset($customer_group_membership_subject_1[$language['language_id']]) ? $customer_group_membership_subject_1[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_subject_1; ?>" class="form-control" />
				  </div>
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-email-1"><?php echo $entry_email_1; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <textarea name="customer_group_membership_email_1[<?php echo $language['language_id']; ?>]" id="email-1-<?php echo $language['language_id']; ?>"><?php echo isset($customer_group_membership_email_1[$language['language_id']]) ? $customer_group_membership_email_1[$language['language_id']] : ''; ?></textarea>
				  <br /><img src="<?php echo version_compare(VERSION, '2.2.0.0', '<') ? 'view/image/flags/' . $language['image'] : 'language/' . $language['code'] . '/' . $language['code'] . '.png'; ?>" title="<?php echo $language['name']; ?>" /><br />
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-duration-1"><?php echo $entry_duration_1; ?></label>
                <div class="col-sm-10">
				  <input type="text" name="customer_group_membership_duration_1" value="<?php echo $customer_group_membership_duration_1; ?>" placeholder="<?php echo $entry_duration_1; ?>" class="form-control" />
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-subject-2"><?php echo $entry_subject_2; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="<?php echo version_compare(VERSION, '2.2.0.0', '<') ? 'view/image/flags/' . $language['image'] : 'language/' . $language['code'] . '/' . $language['code'] . '.png'; ?>" title="<?php echo $language['name']; ?>" /></span>
					<input type="text" name="customer_group_membership_subject_2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($customer_group_membership_subject_2[$language['language_id']]) ? $customer_group_membership_subject_2[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_subject_2; ?>" class="form-control" />
				  </div>
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-email-2"><?php echo $entry_email_2; ?></label>
                <div class="col-sm-10"><?php foreach ($languages as $language) { ?>
                  <textarea name="customer_group_membership_email_2[<?php echo $language['language_id']; ?>]" id="email-2-<?php echo $language['language_id']; ?>"><?php echo isset($customer_group_membership_email_2[$language['language_id']]) ? $customer_group_membership_email_2[$language['language_id']] : ''; ?></textarea>
				  <br /><img src="<?php echo version_compare(VERSION, '2.2.0.0', '<') ? 'view/image/flags/' . $language['image'] : 'language/' . $language['code'] . '/' . $language['code'] . '.png'; ?>" title="<?php echo $language['name']; ?>" /><br />
				  <?php } ?>
				</div>
              </div>
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-duration-2"><?php echo $entry_duration_2; ?></label>
                <div class="col-sm-10">
				  <input type="text" name="customer_group_membership_duration_2" value="<?php echo $customer_group_membership_duration_2; ?>" placeholder="<?php echo $entry_duration_2; ?>" class="form-control" />
				</div>
              </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-rebate"><?php echo $entry_rebate_type; ?></label>
				<div class="col-sm-10">
				  <select name="customer_group_membership_rebate_type" id="input-rebate" class="form-control">
					<option value="0"<?php echo $customer_group_membership_rebate_type == '0' ? ' selected="selected"' : ''; ?>><?php echo $text_none; ?></option>
					<option value="1"<?php echo $customer_group_membership_rebate_type == '1' ? ' selected="selected"' : ''; ?>><?php echo $text_total; ?></option>
					<option value="2"<?php echo $customer_group_membership_rebate_type == '2' ? ' selected="selected"' : ''; ?>><?php echo $text_product; ?></option>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-cron"><?php echo $entry_cron; ?></label>
				<div class="col-sm-10">
				  <textarea rows="3" class="form-control" readonly="true"><?php echo $cron_command; ?></textarea>
				</div>
			  </div>
			  <table id="module" class="table table-striped table-bordered table-hover">
				<thead>
				  <tr>
					<td class="text-right">#</td>
					<td class="text-left"><?php echo $entry_limit; ?></td>
					<td class="text-left"><?php echo $entry_image; ?></td>
					<td></td>
				  </tr>
				</thead>
				<tbody>
				  <?php $module_row = 1; ?>
				  <?php foreach ($customer_group_membership_modules as $customer_group_membership_module) { ?>
				  <tr id="module-row<?php echo $customer_group_membership_module['key']; ?>">
					<td class="text-right"><?php echo $module_row; ?></td>
					<td class="text-left"><input type="text" name="customer_group_membership_module[<?php echo $customer_group_membership_module['key']; ?>][limit]" value="<?php echo $customer_group_membership_module['limit']; ?>" placeholder="<?php echo $entry_limit; ?>" class="form-control" /></td>
					<td class="text-left"><input type="text" name="customer_group_membership_module[<?php echo $customer_group_membership_module['key']; ?>][width]" value="<?php echo $customer_group_membership_module['width']; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
					  <input type="text" name="customer_group_membership_module[<?php echo $customer_group_membership_module['key']; ?>][height]" value="<?php echo $customer_group_membership_module['height']; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
					</td>
					<td class="text-left"><button type="button" onclick="$('#module-row<?php echo $customer_group_membership_module['key']; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
				  </tr>
				  <?php $module_row++; ?>
				  <?php } ?>
				</tbody>
				<tfoot>
				  <tr>
					<td colspan="3"></td>
					<td class="text-left"><button type="button" onclick="addModule();" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
				  </tr>
				</tfoot>
			  </table>
			</div>
			<?php echo $tab; ?>
		  </div>
		</form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#email-1-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#email-2-<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
var token = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tr id="module-row' + token + '">';
	html += '  <td class="text-right">' + token + '</td>';
	html += '  <td class="text-left"><input type="text" name="customer_group_membership_module[' + token + '][limit]" value="5" placeholder="<?php echo $entry_limit; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><input type="text" name="customer_group_membership_module[' + token + '][width]" value="200" placeholder="<?php echo $entry_width; ?>" class="form-control" /> <input type="text" name="customer_group_membership_module[' + token + '][height]" value="200" placeholder="<?php echo $entry_height; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#module-row' + token + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#module tbody').append(html);
	
	token++;
}
//--></script>
<?php echo $footer; ?>