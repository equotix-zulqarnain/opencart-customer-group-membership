<?php
class ControllerModuleCustomerGroupMembership extends Controller {
	public function index($setting) {
		$this->load->language('module/customer_group_membership');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$products_query = $this->db->query("SELECT DISTINCT(product_id) FROM " . DB_PREFIX . "customer_group_to_product");

		$products = array();

		foreach ($products_query->rows as $product) {
			$products[] = $product['product_id'];
		}
		
		shuffle($products);

		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}
	
		$products = array_slice($products, 0, (int)$setting['limit']);

		if ($products) {
			foreach ($products as $product_id) {
				$result = $this->model_catalog_product->getProduct($product_id);
			
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (version_compare(VERSION, '2.2.0.0', '<')) {
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
					} else {
						$tax = false;
					}
				} else {
					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, (version_compare(VERSION, '2.2.0.0', '<') ? $this->config->get('config_product_description_length') : $this->config->get($this->config->get('config_theme') . '_product_description_length'))) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}

			if (version_compare(VERSION, '2.2.0.0', '<')) {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/customer_group_membership.tpl')) {
					return $this->load->view($this->config->get('config_template') . '/template/module/customer_group_membership.tpl', $data);
				} else {
					return $this->load->view('default/template/module/customer_group_membership.tpl', $data);
				}
			} else {
				return $this->load->view('module/customer_group_membership', $data);
			}
		}
	}
}