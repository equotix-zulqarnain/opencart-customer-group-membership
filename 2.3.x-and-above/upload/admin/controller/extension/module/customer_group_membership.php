<?php
require_once(DIR_SYSTEM . 'library/equotix/customer_group_membership/equotix.php');
class ControllerExtensionModuleCustomerGroupMembership extends Equotix {
	protected $version = '2.0.1';
	protected $code = 'customer_group_membership';
	protected $extension = 'Customer Group Membership';
	protected $extension_id = '13';
	protected $purchase_url = 'customer-group-membership';
	protected $purchase_id = '16281';
	protected $error = array();

	public function index() {
		$this->load->language('extension/module/customer_group_membership');
		
		$this->document->addScript('view/javascript/summernote/summernote.js');
		$this->document->addScript('view/javascript/summernote/opencart.js');
		$this->document->addStyle('view/javascript/summernote/summernote.css');

		$this->document->setTitle(strip_tags($this->language->get('heading_title')));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->addModule('customer_group_membership', $this->request->post);
			
			$this->model_setting_setting->editSetting('customer_group_membership', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
		
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
	
		$data['entry_subject_1'] = $this->language->get('entry_subject_1');
		$data['entry_email_1'] = $this->language->get('entry_email_1');
		$data['entry_duration_1'] = $this->language->get('entry_duration_1');
		$data['entry_subject_2'] = $this->language->get('entry_subject_2');
		$data['entry_email_2'] = $this->language->get('entry_email_2');
		$data['entry_duration_2'] = $this->language->get('entry_duration_2');
		$data['entry_cron'] = $this->language->get('entry_cron');
		$data['entry_rebate_type'] = $this->language->get('entry_rebate_type');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled']	= $this->language->get('text_enabled');
		$data['text_disabled']	= $this->language->get('text_disabled');
		$data['text_total'] = $this->language->get('text_total');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_none'] = $this->language->get('text_none');
		
		$data['tab_general'] = $this->language->get('tab_general');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_module_add'] = $this->language->get('button_module_add');
		$data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/customer_group_membership', 'token=' . $this->session->data['token'], true)
   		);
		
		$data['action'] = $this->url->link('extension/module/customer_group_membership', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		
		$data['cron_command'] = 'curl -s -o /dev/null "' . HTTP_CATALOG . 'index.php?route=account/membership/cron&token=0cfa6c079b"';
		
		if (isset($this->request->post['customer_group_membership_subject_1'])) {
			$data['customer_group_membership_subject_1'] = $this->request->post['customer_group_membership_subject_1'];
		} elseif ($this->config->get('customer_group_membership_subject_1')) {
			$data['customer_group_membership_subject_1'] = $this->config->get('customer_group_membership_subject_1');
		} else {
			$data['customer_group_membership_subject_1'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_email_1'])) {
			$data['customer_group_membership_email_1'] = $this->request->post['customer_group_membership_email_1'];
		} elseif ($this->config->get('customer_group_membership_email_1')) {
			$data['customer_group_membership_email_1'] = $this->config->get('customer_group_membership_email_1');
		} else {
			$data['customer_group_membership_email_1'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_duration_1'])) {
			$data['customer_group_membership_duration_1'] = $this->request->post['customer_group_membership_duration_1'];
		} elseif ($this->config->get('customer_group_membership_duration_1')) {
			$data['customer_group_membership_duration_1'] = $this->config->get('customer_group_membership_duration_1');
		} else {
			$data['customer_group_membership_duration_1'] = '';
		}
		
		if (isset($this->request->post['customer_group_membership_subject_2'])) {
			$data['customer_group_membership_subject_2'] = $this->request->post['customer_group_membership_subject_2'];
		} elseif ($this->config->get('customer_group_membership_subject_2')) {
			$data['customer_group_membership_subject_2'] = $this->config->get('customer_group_membership_subject_2');
		} else {
			$data['customer_group_membership_subject_2'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_email_2'])) {
			$data['customer_group_membership_email_2'] = $this->request->post['customer_group_membership_email_2'];
		} elseif ($this->config->get('customer_group_membership_email_2')) {
			$data['customer_group_membership_email_2'] = $this->config->get('customer_group_membership_email_2');
		} else {
			$data['customer_group_membership_email_2'] = array();
		}
		
		if (isset($this->request->post['customer_group_membership_duration_2'])) {
			$data['customer_group_membership_duration_2'] = $this->request->post['customer_group_membership_duration_2'];
		} elseif ($this->config->get('customer_group_membership_duration_2')) {
			$data['customer_group_membership_duration_2'] = $this->config->get('customer_group_membership_duration_2');
		} else {
			$data['customer_group_membership_duration_2'] = '';
		}
		
		if (isset($this->request->post['customer_group_membership_rebate_type'])) {
			$data['customer_group_membership_rebate_type'] = $this->request->post['customer_group_membership_rebate_type'];
		} elseif ($this->config->get('customer_group_membership_rebate_type')) {
			$data['customer_group_membership_rebate_type'] = $this->config->get('customer_group_membership_rebate_type');
		} else {
			$data['customer_group_membership_rebate_type'] = '';
		}
		
		if (isset($this->request->post['customer_group_membership_module'])) {
			$modules = $this->request->post['customer_group_membership_module'];
		} elseif ($this->config->has('customer_group_membership_module')) {
			$modules = $this->config->get('customer_group_membership_module');
		} else {
			$modules = array();
		}
		
		$data['customer_group_membership_modules'] = array();
		
		foreach ($modules as $key => $module) {
			$data['customer_group_membership_modules'][] = array(
				'key'    => $key,
				'limit'  => $module['limit'],
				'width'  => $module['width'],
				'height' => $module['height']
			);
		}
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->generateOutput('extension/module/customer_group_membership', $data);
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/customer_group_membership') || !$this->validated()) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	protected function addModule($code, $data) {
		if (version_compare(VERSION, '2.0.1.0', '>=')) {
			if (isset($data[$code . '_module'])) {
				unset($this->request->post[$code . '_status']);
				
				$this->load->model('extension/module');
				
				$modules = $this->model_extension_module->getModulesByCode($code);
				
				$module_data = array();
				
				foreach ($modules as $module) {
					if (version_compare(VERSION, '2.1.0.0', '>=')) {
						$module_setting = json_decode($module['setting'], true);
					} else {
						$module_setting = unserialize($module['setting']);
					}
					
					if (!empty($module_setting['row_id'])) {
						$module_data[$module_setting['row_id']] = $module['module_id'];
					} else {
						$this->model_extension_module->deleteModule($module['module_id']);
					}
				}
				
				foreach ($data[$code . '_module'] as $key => $value) {
					$module_setting = array();
					
					$module_setting['name'] = 'Module ' . $key;
					$module_setting['status'] = 1;
					$module_setting['row_id'] = $key;
					
					foreach ($value as $key1 => $value1) {
						$module_setting[$key1] = $value1;
					}
					
					if (!empty($module_data[$key])) {
						$this->model_extension_module->editModule($module_data[$key], $module_setting);
						
						unset($module_data[$key]);
					} else {
						$this->model_extension_module->addModule($code, $module_setting);
					}
				}

				foreach ($module_data as $module_id) {
					$this->model_extension_module->deleteModule($module_id);
				}
			}
		} else {
			$this->request->post[$code . '_status'] = 1;
		}
	}
	
	public function install() {
		if (!$this->user->hasPermission('modify', 'extension/extension/module')) {
			return;
		}
		
		$this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD rate decimal(4,2) NOT NULL DEFAULT '0'");
	
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_group_to_product` (
		  `customer_group_to_product_id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_group_id` int(11) NOT NULL,
		  `product_id` int(11) NOT NULL,
		  `product_option_value_id` int(11) NOT NULL,
		  `duration` int(11) NOT NULL,
		  PRIMARY KEY (`customer_group_to_product_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_customer_group` (
		  `order_customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_group_to_product_id` int(11) NOT NULL,
		  `order_id` int(11) NOT NULL,
		  `customer_id` int(11) NOT NULL,
		  `customer_group_id` int(11) NOT NULL,
		  `duration` int(11) NOT NULL,
		  `date_added` datetime NOT NULL,
		  `emailed` tinyint(1) NOT NULL,
		  `expired` tinyint(1) NOT NULL,
		  PRIMARY KEY (`order_customer_group_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		
		$this->load->model('setting/setting');
		
		$subject_1[1] = '{customer_group} Membership Expiring in 10 Days';
		$email_1[1] = '<p>Dear {firstname} {lastname},</p>

<p>Your {customer_group} membership will expire on {expires}. We would recommend you to extend your membership with us as soon as possible.</p>

<p>Best regards,<br />
' . $this->config->get('config_name') . '</p>
';

		$subject_2[1] = '{customer_group} Membership Expiring in 2 Days';
		$email_2[1] = '<p>Dear {firstname} {lastname},</p>

<p>Your {customer_group} membership will expire on {expires}. We would recommend you to extend your membership with us as soon as possible.</p>

<p>Best regards,<br />
' . $this->config->get('config_name') . '</p>
';
		
		$data = array(
			'customer_group_membership_subject_1'	=> $subject_1,
			'customer_group_membership_email_1'		=> $email_1,
			'customer_group_membership_duration_1'	=> '10',
			'customer_group_membership_subject_2'	=> $subject_2,
			'customer_group_membership_email_2'		=> $email_2,
			'customer_group_membership_duration_2'	=> '2',
		);
		
		$this->model_setting_setting->editSetting('customer_group_membership', $data);
	}
	
	public function uninstall() {
		if (!$this->user->hasPermission('modify', 'extension/extension/module')) {
			return;
		}
		
		$this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group DROP rate");

		$this->db->query("DROP TABLE `" . DB_PREFIX . "customer_group_to_product`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "order_customer_group`");
	}
}